const table = document.querySelector('.table');


fetch('https://treinamento-api.herokuapp.com/books').then(response => response.json()).then(responseArray => {
    for(let i =0; i < responseArray.length; i++){
        let tableRow = document.createElement('tr');
        let tableColID = document.createElement('th');
        let tableColName = document.createElement('th');
        let tableColAuthor = document.createElement('th');
        let tableColCrt = document.createElement('th');
        let tableColUpd = document.createElement('th');

        tableColID.innerText = responseArray[i].id;
        tableColName.innerText = responseArray[i].name;
        tableColAuthor.innerText = responseArray[i].author;
        tableColCrt.innerText = responseArray[i].created_at;
        tableColUpd.innerText = responseArray[i].updated_at;

        tableRow.appendChild(tableColID);
        tableRow.appendChild(tableColName);
        tableRow.appendChild(tableColAuthor);
        tableRow.appendChild(tableColCrt);
        tableRow.appendChild(tableColUpd);

        table.appendChild(tableRow);
    }
})


const botaoFormulario = document.querySelector('.form button');
const inputBookName = document.querySelector('.bookName');
const inputBookAuthor = document.querySelector('.bookAuthor');

botaoFormulario.addEventListener('click', (e) => {
    let name = `${inputBookName.value}`;
    let author = `${inputBookAuthor.value}`;
    let novoLivro = {
        "book": {
            "name": String(name),
            "author": String(author)
        }
    };
    fetch('https://treinamento-api.herokuapp.com/books', {
        method: 'POST',
	    headers: {"content-Type": "application/json"},
	    body: JSON.stringify(novoLivro)
    })
    .then(response => response.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', JSON.stringify(response)));

    e.preventDefault();
});


